class RPNCalculator
  # TODO: your code goes here!
  def initialize
    @arr = []
  end

  def push(el)
    @arr << el
  end

  def pop_twice(array)
    2.times { array.pop }
  end

  def raise_error(array)
    raise "calculator is empty" if array.empty?
  end

  def plus
    raise_error(@arr)
    sum = @arr[-2] + @arr[-1]
    pop_twice(@arr)
    @arr.push(sum)
  end

  def minus
    raise_error(@arr)
    subtract = @arr[-2] - @arr[-1]
    pop_twice(@arr)
    @arr.push(subtract)
  end

  def divide
    raise_error(@arr)
    factor = @arr[-2].to_f / @arr[-1].to_f
    pop_twice(@arr)
    @arr.push(factor)
  end

  def times
    raise_error(@arr)
    product = @arr[-2].to_f * @arr[-1].to_f
    pop_twice(@arr)
    @arr.push(product)
  end

  def value
    @arr[-1]
  end


  def tokens(string)
    operators = "+-*/"
    string.split.map do |ele|
      if operators.include?(ele)
        ele.to_sym
      else
        ele.to_i
      end
    end
  end

  def evaluate(string)
    new_arr = tokens(string)
    new_arr.map.with_index do |ele|
      if ele == :*
        times
      elsif ele == :+
        plus
      elsif ele == :-
        minus
      elsif ele == :/
        divide
      else
        push(ele)
      end
    end

    value
  end

end
